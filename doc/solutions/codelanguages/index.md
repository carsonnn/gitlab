---
stage: Solutions Architecture
group: Solutions Architecture
info: This page is owned by the Solutions Architecture team.
---

# Coding Languages

This section provides resource indexes and enablements around individual coding languages including any ecosystem technologies that surround them (e.g. dependency packaging frameworks)

## Relationship to documentation

While information in this section gives valuable and qualified guidance on ways to solve problems by using the GitLab platform, the product documentation is the authoritative reference for product features and functions.

## Solutions categories

[Rust Language Solutions](rust/index.md)
